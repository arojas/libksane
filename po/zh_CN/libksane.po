msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-24 00:40+0000\n"
"PO-Revision-Date: 2024-03-14 19:30\n"
"Last-Translator: \n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Project-ID: 269464\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/libksane/libksane.pot\n"
"X-Crowdin-File-ID: 5785\n"

#: ../ksanetwain/ktwain_widget.cpp:29
#, kde-format
msgid "Open scan dialog"
msgstr "打开扫描对话框"

#: i18n-hplip.cpp:3
#, kde-format
msgid "None"
msgstr "无"

#: i18n-hplip.cpp:4
#, kde-format
msgid "MH"
msgstr "MH"

#: i18n-hplip.cpp:5
#, kde-format
msgid "MR"
msgstr "MR"

#: i18n-hplip.cpp:6
#, kde-format
msgid "MMR"
msgstr "MMR"

#: i18n-hplip.cpp:7
#, kde-format
msgid "JPEG"
msgstr "JPEG"

#: i18n-hplip.cpp:8
#, kde-format
msgid "Auto"
msgstr "自动"

#: i18n-hplip.cpp:9
#, kde-format
msgid "Flatbed"
msgstr "平台型"

#: i18n-hplip.cpp:10
#, kde-format
msgid "ADF"
msgstr "自动送纸器"

#: i18n-hplip.cpp:11
#, kde-format
msgid "Advanced"
msgstr "高级"

#: i18n-hplip.cpp:12
#, kde-format
msgid "Compression"
msgstr "压缩"

#: i18n-hplip.cpp:13
#, kde-format
msgid ""
"Selects the scanner compression method for faster scans, possibly at the "
"expense of image quality."
msgstr ""
"选择扫描仪的图像压缩方式，压缩率越大，扫描速度越快，可能会降低图像品质。"

#: i18n-hplip.cpp:15
#, kde-format
msgid "JPEG compression factor"
msgstr "JPEG 压缩选项"

#: i18n-hplip.cpp:16
#, kde-format
msgid ""
"Sets the scanner JPEG compression factor. Larger numbers mean better "
"compression, and smaller numbers mean better image quality."
msgstr ""
"设定扫描仪的 JPEG 压缩选项。数值越高，压缩率越大；数值越小，图像品质越高。"

#: i18n-hplip.cpp:19
#, kde-format
msgid "Batch scan"
msgstr "批量扫描"

#: i18n-hplip.cpp:20
#, kde-format
msgid "Enables continuous scanning with automatic document feeder (ADF)."
msgstr "启用自动送纸器 (ADF) 连续扫描。"

#: i18n-hplip.cpp:21
#, kde-format
msgid "Duplex"
msgstr "双面"

#: i18n-hplip.cpp:22
#, kde-format
msgid "Enables scanning on both sides of the page."
msgstr "启用双面扫描。"

#: i18n-hplip.cpp:23
#, kde-format
msgid "Geometry"
msgstr "尺寸"

#: i18n-hplip.cpp:24
#, kde-format
msgid "Length measurement"
msgstr "长度测量方式"

#: i18n-hplip.cpp:25
#, kde-format
msgid ""
"Selects how the scanned image length is measured and reported, which is "
"impossible to know in advance for scrollfed scans."
msgstr "选择扫描图像长度的测量和报告方式，滚动扫描时无法提前得知结果。"

#: i18n-hplip.cpp:27
#, kde-format
msgid "Unknown"
msgstr "未知"

#: i18n-hplip.cpp:28
#, kde-format
msgid "Unlimited"
msgstr "无限制"

#: i18n-hplip.cpp:29
#, kde-format
msgid "Approximate"
msgstr "估计"

#: i18n-hplip.cpp:30
#, kde-format
msgid "Padded"
msgstr "留空"

#: i18n-hplip.cpp:31
#, kde-format
msgid "Exact"
msgstr "精确"

#: i18n-hplip.cpp:32
#, kde-format
msgid "???"
msgstr "???"

#: ksanedevicedialog.cpp:36
#, kde-format
msgid ""
"<html>The SANE (Scanner Access Now Easy) system could not find any device."
"<br>Check that the scanner is plugged in and turned on<br>or check your "
"systems scanner setup.<br>For details about SANE see the <a href='http://www."
"sane-project.org/'>SANE homepage</a>.</html>"
msgstr ""
"<html>SANE (Scanner Access Now Easy) 系统无法找到任何设备。<br>请检查扫描仪是"
"否已连接并开启<br>或检查您的系统扫描仪设置。<br>有关 SANE 的详情请参见 <a "
"href='http://www.sane-project.org/'>SANE 主页</a>。</html>"

#: ksanedevicedialog.cpp:58
#, kde-format
msgid "Reload devices list"
msgstr "刷新设备列表"

#: ksanedevicedialog.cpp:82
#, kde-format
msgid "Looking for devices. Please wait."
msgstr "正在搜索设备。请稍候。"

#: ksanedevicedialog.cpp:117
#, kde-format
msgid "Sorry. No devices found."
msgstr "对不起，没有发现任何设备。"

#: ksanedevicedialog.cpp:127
#, kde-format
msgid "Found devices:"
msgstr "已发现设备："

#: ksaneviewer.cpp:109 ksanewidget.cpp:86
#, kde-format
msgid "Zoom In"
msgstr "放大"

#: ksaneviewer.cpp:112 ksanewidget.cpp:92
#, kde-format
msgid "Zoom Out"
msgstr "缩小"

#: ksaneviewer.cpp:115 ksanewidget.cpp:98
#, kde-format
msgid "Zoom to Selection"
msgstr "缩放至选中区域"

#: ksaneviewer.cpp:118 ksanewidget.cpp:104
#, kde-format
msgid "Zoom to Fit"
msgstr "缩放至适合大小"

#: ksaneviewer.cpp:121 ksanewidget.cpp:110
#, kde-format
msgid "Clear Selections"
msgstr "清空选中区域"

#: ksanewidget.cpp:58
#, kde-format
msgid "Waiting for the scan to start."
msgstr "正在等待扫描开始。"

#: ksanewidget.cpp:71
#, kde-format
msgid "Cancel current scan operation"
msgstr "取消当前扫描操作"

#: ksanewidget.cpp:121
#, kde-format
msgid "Scan Preview Image (%1)"
msgstr "扫描预览图像 (%1)"

#: ksanewidget.cpp:122
#, kde-format
msgctxt "Preview button text"
msgid "Preview"
msgstr "预览"

#: ksanewidget.cpp:127
#, kde-format
msgid "Scan Final Image (%1)"
msgstr "扫描最终图像 (%1)"

#: ksanewidget.cpp:128
#, kde-format
msgctxt "Final scan button text"
msgid "Scan"
msgstr "扫描"

#: ksanewidget.cpp:169
#, kde-format
msgid "Basic Options"
msgstr "基本选项"

#: ksanewidget.cpp:175
#, kde-format
msgid "Advanced Options"
msgstr "高级选项"

#: ksanewidget.cpp:181
#, kde-format
msgid "Scanner Specific Options"
msgstr "扫描仪特有选项"

#: ksanewidget.cpp:278
#, kde-format
msgid "Authentication required for resource: %1"
msgstr "资源需要验证：%1"

#: ksanewidget_p.cpp:410
#, kde-format
msgid "Scan Area Size"
msgstr "扫描区域大小"

#: ksanewidget_p.cpp:415
#, kde-format
msgid " mm"
msgstr " 毫米"

#: ksanewidget_p.cpp:415
#, kde-format
msgid " inch"
msgstr " 英寸"

#: ksanewidget_p.cpp:417
#, kde-format
msgid "Width"
msgstr "宽度"

#: ksanewidget_p.cpp:422
#, kde-format
msgid "Height"
msgstr "高度"

#: ksanewidget_p.cpp:427
#, kde-format
msgid "X Offset"
msgstr "X 偏移"

#: ksanewidget_p.cpp:432
#, kde-format
msgid "Y Offset"
msgstr "Y 偏移"

#: ksanewidget_p.cpp:493
#, kde-format
msgid "Image intensity"
msgstr "图像亮度"

#: ksanewidget_p.cpp:497
#, kde-format
msgid ""
"Gamma-correction table. In color mode this option equally affects the red, "
"green, and blue channels simultaneously (i.e., it is an intensity gamma "
"table)."
msgstr "伽玛校正表。在彩色模式下，此选项将同时对红、绿、蓝通道发挥同等影响。"

#: ksanewidget_p.cpp:505
#, kde-format
msgid "Separate color intensity tables"
msgstr "拆分颜色亮度表"

#: ksanewidget_p.cpp:1122
#, kde-format
msgctxt "This is compared to the option string returned by sane"
msgid "Transparency"
msgstr "透明度"

#: ksanewidget_p.cpp:1124
#, kde-format
msgctxt "This is compared to the option string returned by sane"
msgid "Negative"
msgstr "负片"

#: ksanewidget_p.cpp:1171
#, kde-format
msgid "Next scan starts in %1 s."
msgstr "下一次扫描将在 %1 秒后开始。"

#: ksanewidget_p.cpp:1184
#, kde-format
msgctxt "@title:window"
msgid "General Error"
msgstr "常规错误"

#: ksanewidget_p.cpp:1187
#, kde-format
msgctxt "@title:window"
msgid "Information"
msgstr "信息"

#: ksanewidget_p.cpp:1269
#, kde-format
msgid "Custom"
msgstr "自定义"

#: ksanewidget_p.cpp:1294
#, kde-format
msgctxt "Page size landscape"
msgid " Landscape"
msgstr " 横向"

#: widgets/ksaneoptionwidget.cpp:66
#, kde-format
msgctxt "Label for a scanner option"
msgid "%1:"
msgstr "%1："

#: widgets/labeledcombo.cpp:155 widgets/labeledcombo.cpp:186
#, kde-format
msgctxt "Parameter and Unit"
msgid "%1 Pixel"
msgid_plural "%1 Pixels"
msgstr[0] "%1 像素"

#: widgets/labeledcombo.cpp:158 widgets/labeledcombo.cpp:189
#, kde-format
msgctxt "Parameter and Unit"
msgid "%1 Bit"
msgid_plural "%1 Bits"
msgstr[0] "%1 位"

#: widgets/labeledcombo.cpp:161 widgets/labeledcombo.cpp:192
#, kde-format
msgctxt "Parameter and Unit (Millimeter)"
msgid "%1 mm"
msgstr "%1 毫米"

#: widgets/labeledcombo.cpp:164 widgets/labeledcombo.cpp:195
#, kde-format
msgctxt "Parameter and Unit (Dots Per Inch)"
msgid "%1 DPI"
msgstr "%1 DPI"

#: widgets/labeledcombo.cpp:167 widgets/labeledcombo.cpp:198
#, kde-format
msgctxt "Parameter and Unit (Percentage)"
msgid "%1 %"
msgstr "%1 %"

#: widgets/labeledcombo.cpp:170 widgets/labeledcombo.cpp:201
#, kde-format
msgctxt "Parameter and Unit (Microseconds)"
msgid "%1 µs"
msgstr "%1 微米"

#: widgets/labeledcombo.cpp:173 widgets/labeledcombo.cpp:204
#, kde-format
msgctxt "Parameter and Unit (seconds)"
msgid "%1 s"
msgstr "%1 秒"

#: widgets/labeledcombo.cpp:176 widgets/labeledcombo.cpp:207
#, kde-format
msgid "%1"
msgstr "%1"

#: widgets/labeledentry.cpp:52
#, kde-format
msgctxt "Label for button to reset text in a KLineEdit"
msgid "Reset"
msgstr "重置"

#: widgets/labeledentry.cpp:54
#, kde-format
msgctxt "Label for button to write text in a KLineEdit to sane"
msgid "Set"
msgstr "设定"

#: widgets/labeledfslider.cpp:40
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit"
msgid " Pixels"
msgstr " 像素"

#: widgets/labeledfslider.cpp:43
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit"
msgid " Bits"
msgstr " 位"

#: widgets/labeledfslider.cpp:46
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit (Millimeter)"
msgid " mm"
msgstr " 毫米"

#: widgets/labeledfslider.cpp:49
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit (Dots Per Inch)"
msgid " DPI"
msgstr " DPI"

#: widgets/labeledfslider.cpp:52
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit (Percentage)"
msgid " %"
msgstr " %"

#: widgets/labeledfslider.cpp:55
#, kde-format
msgctxt "Double numbers. SpinBox parameter unit (Microseconds)"
msgid " µs"
msgstr " 微米"

#: widgets/labeledfslider.cpp:58
#, kde-format
msgctxt "SpinBox parameter unit (seconds), float"
msgid " s"
msgstr " 秒"

#: widgets/labeledgamma.cpp:43
#, kde-format
msgid "Brightness"
msgstr "亮度"

#: widgets/labeledgamma.cpp:46
#, kde-format
msgid "Contrast"
msgstr "对比度"

#: widgets/labeledgamma.cpp:49
#, kde-format
msgid "Gamma"
msgstr "Gamma"

#: widgets/labeledslider.cpp:42
#, kde-format
msgctxt "SpinBox parameter unit"
msgid " Pixel"
msgid_plural " Pixels"
msgstr[0] " 像素"

#: widgets/labeledslider.cpp:45
#, kde-format
msgctxt "SpinBox parameter unit"
msgid " Bit"
msgid_plural " Bits"
msgstr[0] " 位"

#: widgets/labeledslider.cpp:48
#, kde-format
msgctxt "SpinBox parameter unit (Millimeter)"
msgid " mm"
msgid_plural " mm"
msgstr[0] " 毫米"

#: widgets/labeledslider.cpp:51
#, kde-format
msgctxt "SpinBox parameter unit (Dots Per Inch)"
msgid " DPI"
msgid_plural " DPI"
msgstr[0] " DPI"

#: widgets/labeledslider.cpp:54
#, kde-format
msgctxt "SpinBox parameter unit (Percentage)"
msgid " %"
msgid_plural " %"
msgstr[0] " %"

#: widgets/labeledslider.cpp:57
#, kde-format
msgctxt "SpinBox parameter unit (Microseconds)"
msgid " µs"
msgid_plural " µs"
msgstr[0] " 微秒"

#: widgets/labeledslider.cpp:60
#, kde-format
msgctxt "SpinBox parameter unit (seconds)"
msgid " s"
msgid_plural " s"
msgstr[0] " 秒"
